<?php get_header(); ?>
<?php setpostview(get_the_id()); ?>
  <!--blog content-->
  <!--blog content-->
  <div class="blog-main__content">

    <div class="blog__detail">
      <div class="blog__detail__content">
        <div class="block_full">
        <!-- post content-->
        <?php
          // Start the loop.
          while ( have_posts() ) : the_post(); ?>
            <div class="blog__detail__header">
              <div class="card__tag-post single-page">
                <?php the_tags( ' ', '  ', '<br />' ); ?>
              </div>
              <h1 class="single__h1"><?php the_title(); ?></h1>
              <!--more-->
              <div class="blog-main__post-ico">
                <div class="post-icon">
                   <?php
                      $category_detail=get_the_category(get_the_id());//$post->ID
                      foreach($category_detail as $cd){
                   ?>
                       <img src="<?php echo z_taxonomy_image_url($cd->term_id); ?>" class="icon-post" alt="icon category"/>
                  <?php } ?>
                </div>
                <span class="post-time">
                  <?php
                   $post_d = get_the_date( 'j' );
                   $post_m = get_the_date( 'm' );
                   $post_y = get_the_date( 'Y' );
                   if($post_d > 9){
                     echo $post_d;
                   }else echo '0'.$post_d;
                   echo ' - '.$post_m;
                   echo ' - '.$post_y ;
                 ?>
                </span>
                <span class="post-line">|</span>
                <span class="post-view-counter">
                  Lượt xem :
                  <span style="color: #20b4c9; margin-left: 5px;">
                    <?php echo getpostviews(get_the_id()); ?>
                  </span>
                </span>
              </div>
              <!--end more-->

            </div>

            <!--img feature-->
            <?php //if (has_post_thumbnail( $post->ID ) ): ?>
              <?php //$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
              <!--div class="blog-main__detail-post-avata">
                <img src="<?php //echo $image[0]; ?>" alt="img post" />
              </div-->
            <?php //endif; ?>
            <!--img feature-->

            <!--content-->
            <div class="blog-detail">
              <?php the_content(); ?>
            </div>
            <!--e content-->
            <div class="tag-post">
              <?php the_tags( ' ', '  ', '<br />' ); ?>
            </div>
          <?php
          endwhile;
        ?>
        <!--end post content-->
        <div class="post-comments">
          <div class="post-comments-title">
            <img src="http://phantrongbien.com/wp-content/uploads/2018/06/chat.png" alt="icon-comments" />
            <span>Gửi bình luận</span>
          </div>
          <?php
          // If comments are open or we have at least one comment, load up the comment template.
          if ( comments_open() || get_comments_number() ) :
              comments_template();
          endif;
          ?>
        </div>
        </div>
      </div>

      <!--next post-->
      <!--div class="next-post">
        <a href="blog_detail.html" class="next-post__left">
          <img src="images/left-arrow.svg" alt="button prev" class="icon-arrow" />
          Bài trước
        </a>

        <a href="blog_detail.html" class="next-post__right">
          Bài tiếp theo
          <img src="images/right-arrow.svg" alt="button next" class="icon-arrow" />
        </a>
      </div>
      <!end next post-->

      <!--list right-->
      <div class="blog__detail__list">
        <div class="list__title">
          <span>ALL RELATED POST</span>
        </div>
        <?php
          $categories = get_the_category($post->ID);
          if ($categories)
            {
              $category_ids = array();
              foreach($categories as $individual_category) $category_ids[] = $individual_category->term_id;

              $args=array(
              'category__in' => $category_ids,
              'post__not_in' => array($post->ID), // bỏ qua bài viết hiện tại
              'showposts'=>99,
              'caller_get_posts'=>1
          );
          $my_query = new wp_query($args);
            if( $my_query->have_posts() )
              {
                while ($my_query->have_posts())
                  {
                    $my_query->the_post();
                    ?>
                    <!-- blog items -->
                    <div class="list__items">
                      <a href="<?php the_permalink() ?>" title="<?php the_title(); ?>">
                        <?php the_title(); ?>
                      </a>
                      <!--more-->
                      <div class="blog-main__post-ico side-list">
                        <div class="post-icon">
                           <?php
                              $category_detail=get_the_category(get_the_id());//$post->ID
                              foreach($category_detail as $cd){
                           ?>
                               <img src="<?php echo z_taxonomy_image_url($cd->term_id); ?>" class="icon-post" alt="icon category"/>
                          <?php } ?>
                        </div>
                        <span class="post-time">
                          <?php
                           $post_d = get_the_date( 'j' );
                           $post_m = get_the_date( 'm' );
                           $post_y = get_the_date( 'Y' );
                           if($post_d > 9){
                             echo $post_d;
                           }else echo '0'.$post_d;
                           echo ' - '.$post_m;
                           echo ' - '.$post_y ;
                         ?>
                        </span>
                        <span class="post-line">|</span>
                        <span class="post-view-counter">
                          Lượt xem :
                          <span style="color: #20b4c9; margin-left: 5px;">
                            <?php echo getpostviews(get_the_id()); ?>
                          </span>
                        </span>
                      </div>
                      <!--end more-->
                    </div>
                    <!--e blog items-->
                  <?php
                }
            }
        }
      ?>
      </div>
      <!--e list right-->
    </div>

  </div>
  <!--end blog content-->
  <!--end blog content-->
<?php get_footer(); ?>
