var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync');
var useref = require('gulp-useref');
var uglify = require('gulp-uglify');
var gulpIf = require('gulp-if');
var cssnano = require('gulp-cssnano');
var imagemin = require('gulp-imagemin');
var cache = require('gulp-cache');
var del = require('del');
var runSequence = require('run-sequence');

// Development Tasks

// Start browserSync server
gulp.task('browserSync', function() {
  browserSync({
    server: {
      baseDir: 'assets',
    }
  })
})

// Sass compile
gulp.task('sass', function() {
  return gulp.src('assets/scss/**/*.scss') // Gets all files ending with .scss in assets/scss and children dirs
    .pipe(sass().on('error', sass.logError)) // Passes it through a gulp-sass, log errors to console
    .pipe(gulp.dest('assets/css'))
    .pipe(browserSync.reload({ // Reloading with Browser Sync
      stream: true
    }));
})

// Optimization Tasks
// ------------------

// Optimizing CSS and JavaScript
gulp.task('build', function() {
  return gulp.src('assets/css/**/*.css')
    .pipe(useref())
    .pipe(gulpIf('*.css', cssnano()))
    .pipe(gulp.dest('assets/css'))
    .pipe(browserSync.reload({ // Reloading with Browser Sync
      stream: true
    }));
})

// Watchers
gulp.task('watch', function() {
  gulp.watch('assets/scss/**/*.scss', ['sass']);
  gulp.watch('assets/scss/**/*.scss', browserSync.reload);
  gulp.watch('assets/**/*.html', browserSync.reload);
  gulp.watch('assets/**/*.js', browserSync.reload);
  gulp.watch('assets/**/*.css', browserSync.reload);
  gulp.watch('assets/images/**/*.+(png|jpg|jpeg|gif|svg)', browserSync.reload);
})


gulp.task('run', function(callback) {
  runSequence(['sass', 'browserSync'], 'watch',
    callback
  )
})
