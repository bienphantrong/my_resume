<div class="col-md-12">
  <div class="post__wrapper">
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
      <header class="post__entry">
        <div class="post__time">
          <span><i class="fa fa-clock-o" aria-hidden="true"></i>Thời gian : </span><?php blog_entry_meta() ?>
        </div>
        <div class="post__link">
          <?php blog_entry_header(); ?>
        </div>
      </header>
      <div class="post__content">
        <?php
          ( is_single() ? blog_thumbnail(full) : '' );
          blog_entry_content();?>
        <?php ( is_single() ? blog_entry_tag() : '' ); ?>
      </div>
    </article>
  </div>
</div>
