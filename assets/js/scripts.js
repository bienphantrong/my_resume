/*
Theme Name: PHAN TRONG BIEN
Author: PHAN TRONG BIEN
Author URL: phantrongbien.com
*/

$(document).ready(function () {

  // ********************************************
  /*
  MENU RESPONSIVE
  */
  // ********************************************
  var menu     = document.getElementsByClassName("menu-responsive")[0];
  var menuBtn  = document.getElementsByClassName("menuBtn")[0];
  var closeBtn = document.getElementsByClassName("hideBtn")[0];

  menuBtn.addEventListener("click",openMenu);

  function openMenu()
  {
    menu.classList.add("menuOpened");
    menuBtn.classList.add("hideBtn");
    closeBtn.classList.add("closeBtn");
  }

  closeBtn.addEventListener("click",hideMenu);

  function hideMenu()
  {
    menu.classList.remove("menuOpened");
    menuBtn.classList.remove("hideBtn");
    closeBtn.classList.remove("closeBtn");
  }

  // ********************************************
  // SKILL BAR
  // ********************************************
  $(".meter").filter(function( index ) {
    var bar = $(this).find('span');
    var p = $(this).find('p');

    var width = bar.attr('style');
    width = width.replace("width:", "");
    width = width.substr(0, width.length-1);


    var interval;
    var start = 0;
    var end = parseInt(width);
    var current = start;

    var countUp = function() {
      current++;
      p.html(current + "%");

      if (current === end) {
        clearInterval(interval);
      }
    };
    interval = setInterval(countUp, (1000 / (end + 1)));
  })

  // ********************************************
  // ***** Timeline
  // ********************************************
  

});
