<?php
  /**
  * THEME_URL = get_stylesheet_directory() - url toi thu muc Theme
  * CORE = thu muc /core cua theme, chua cac file nguon quan trong
  **/
  define( 'THEME_URL', get_stylesheet_directory() );
  define( 'CORE', THEME_URL . '/core' );
  define( 'icon-design', get_template_directory_uri() . '/assets/images/esign-icon.svg');
  /**
  *
  **/
  /**
  * Thiet lap co ban
  **/
  if ( ! function_exists( 'blog_theme_setup' ) ) {
  /*
  * Nếu chưa có hàm blog_theme_setup() thì sẽ tạo mới hàm đó
  */
    function blog_theme_setup() {
    /*
    * Thiết lập theme có thể dịch được
    */
      $language_folder = THEME_URL . '/languages';
      load_theme_textdomain( 'blog_language', $language_folder );

      /*
      * Tự chèn RSS Feed links trong <head>
      */
      add_theme_support( 'automatic-feed-links' );

      /*
      * Thêm chức năng post thumbnail
      */
      add_theme_support( 'post-thumbnails' );

      /*
      * Thêm chức năng title-tag để tự thêm <title>
      */
      add_theme_support( 'title-tag' );

      /*
      * Thêm chức năng post format
      */
      add_theme_support( 'post-formats',
        array(
          'video',
          'image',
          'audio',
          'gallery'
        )
      );

      /*
      * Thêm chức năng custom background
      */
      $default_background = array(
        'default-color' => '#e8e8e8',
      );
      add_theme_support( 'custom-background', $default_background );

      /*
      * Tạo menu cho theme
      */
      register_nav_menu ( 'primary-menu', __('Primary Menu', 'blog_language') );

      /*
      * Tạo sidebar cho theme
      */
      $sidebar = array(
        'name' => __('Main Sidebar', 'blog_language'),
        'id' => 'main-sidebar',
        'description' => 'Main sidebar for blog theme',
        'class' => 'main-sidebar',
        'before_title' => '<h3 class="widgettitle">',
        'after_sidebar' => '</h3>'
      );
      register_sidebar( $sidebar );
    }
    add_action ( 'init', 'blog_theme_setup' );
  }
  /**

  * Logo main
  **/

  function theme_prefix_setup() {

  	add_theme_support( 'custom-logo', array(
  		'height'      => 100,
  		'width'       => 400,
  		'flex-width' => true,
  	) );

  }
  add_action( 'after_setup_theme', 'theme_prefix_setup' );

  function theme_prefix_the_custom_logo() {

  	if ( function_exists( 'the_custom_logo' ) ) {
  		the_custom_logo();
  	}

  }

  if ( ! function_exists( 'blog_logo' ) ) {
    function blog_logo() {?>
      <?php if ( is_home() ) {
          printf(
              '<a href="%1$s" title="%2$s"><image src="%3$s" alt="%4$s" class="logo__img" /></a>',
              get_bloginfo( 'url' ),
              get_bloginfo( 'description' ),
              LOGO_URL,
              get_bloginfo( 'sitename' )
            );
          } else {
          printf(
              '<p><a href="%1$s" title="%2$s"><image src="%3$s" alt="%4$s" class="logo__img" /></a></p>',
              get_bloginfo( 'url' ),
              get_bloginfo( 'description' ),
              LOGO_URL,
              get_bloginfo( 'sitename' )
            );
          } // endif ?>

    <?php }
  }

  /*** Hidden Post has passwork ***/
  // Hide protected posts

  function exclude_protected($where) {
      global $wpdb;
      return $where .= " AND {$wpdb->posts}.post_password = '' ";
  }

  // Where to display protected posts
  function exclude_protected_action($query) {
      if( !is_single() && !is_page() && !is_admin() ) {
          add_filter( 'posts_where', 'exclude_protected' );
      }
  }

  // Action to queue the filter at the right time
  add_action('pre_get_posts', 'exclude_protected_action');
  /**
  * Thiết lập hàm hiển thị menu
  * blog_menu( $slug )
  **/
  if ( ! function_exists( 'blog_menu' ) ) {
    function blog_menu( $slug ) {
      $menu = array(
        'theme_location' => $slug,
        'container' => 'nav',
        'container_class' => $slug,
      );
      wp_nav_menu( $menu );
    }
  }
  /**
  * Hàm hiển thị ảnh thumbnail của post.
  * Ảnh thumbnail sẽ không được hiển thị trong trang single
  * Nhưng sẽ hiển thị trong single nếu post đó có format là Image
  * blog_thumbnail( $size )
  **/
  if ( ! function_exists( 'blog_thumbnail' ) ) {
    function blog_thumbnail( $size ) {
    // Chỉ hiển thumbnail với post không có mật khẩu
    if ( is_single() &&  has_post_thumbnail()  && ! post_password_required() || has_post_format( 'image' ) ) : ?>
      <div class="post-featured"><figure><?php the_post_thumbnail( $size ); ?></figure></div><?php
    endif;
    }
  }
  /**
  * Hàm hiển thị tiêu đề của post trong .entry-header
  * Tiêu đề của post sẽ là nằm trong thẻ <h1> ở trang single
  * Còn ở trang chủ và trang lưu trữ, nó sẽ là thẻ <h2>
  * blog_entry_header()
  **/
  if ( ! function_exists( 'blog_entry_header' ) ) {
    function blog_entry_header() {
      if ( is_single() ) : ?>
        <h1 class="entry-title">
          <?php the_title(); ?>
        </h1>
      <?php else : ?>
        <h2 class="entry-title">
          <a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
            <?php the_title(); ?>
          </a>
        </h2><?php
      endif;
    }
  }

  /**
  * Hàm hiển thị thông tin của post (Post Meta)
  * blog_entry_meta()
  **/
  if( ! function_exists( 'blog_entry_meta' ) ) {
    function blog_entry_meta() {
      if ( ! is_page() ) :
        echo '<span class="entry-meta">';
          $post_date = get_the_date( 'j/m/' );
          $post_y = get_the_date( 'Y' );
          echo $post_date ;
          echo $post_y ;
        echo '</span>';
      endif;
    }
  }

  /* hien thi meta thu gon */
  if( ! function_exists( 'blog_entry_meta_small' ) ) {
    function blog_entry_meta_small() {
      if ( ! is_page() ) :
        echo '<div class="entry-meta">';
          // Hiển thị tên tác giả, tên category và ngày tháng đăng bài
          $post_date = get_the_date( 'j/m/' );
          $post_y = get_the_date( 'Y' );
          echo '<div class="time-top">';
            echo $post_date ;
          echo '</div>';
          echo '<div class="time-bottom">';
            echo $post_y ;
          echo '</div>';
        echo '</div>';
      endif;
    }
  }

  /**
  *Tạo hàm phân trang cho index, archive.
  **/
  if ( ! function_exists( 'blog_pagination' ) ) {
    function blog_pagination() {
      /*
       * Không hiển thị phân trang nếu trang đó có ít hơn 2 trang
       */
      if ( $GLOBALS['wp_query']->max_num_pages < 3 ) {
        return '';
      }
    ?>

    <nav class="pagination" role="navigation">
        <div class="prev"><?php next_posts_link( __('[ Trang sau ]', 'blog-language') ); ?></div>
        <div class="next"><?php previous_posts_link( __('[ Trang trước ]', 'blog-language') ); ?></div>
    </nav><?php
    }
  }

  /* Hàm hiển thị nội dung của post type
  * Hàm này sẽ hiển thị đoạn rút gọn của post ngoài trang chủ (the_excerpt)
  * Nhưng nó sẽ hiển thị toàn bộ nội dung của post ở trang single (the_content)
  * blog_entry_content()
  **/
  if ( ! function_exists( 'blog_entry_content' ) ) {
    function blog_entry_content() {
      if ( ! is_single() ) :
        the_excerpt();
      else :
      the_content();
      /*
      * Code hiển thị phân trang trong post type
      */
          $link_pages = array(
            'before' => __('<p>Page:', 'blog-language'),
            'after' => '</p>',
            'nextpagelink'     => __( 'Next page', 'blog-language' ),
            'previouspagelink' => __( 'Previous page', 'blog-language' )
          );
          wp_link_pages( $link_pages );
        endif;
      }
    }

    /* set single */
      // Định nghĩa đường dẫn mặc định cho folder template single
        //define( 'SINGLE_PATH', THEME_URL ."/templates" );

        // Lọc single_template bằng custom function
        //add_filter('single_template', 'my_single_template');

        // Function template single chọn template
        // function my_single_template($single)
        // {
        //      global $wp_query, $post;
        //
        //      // Kiểm tra template single theo category
        //      // Kiểm tra category slug và ID
        //      foreach((array)get_the_category() as $cat) :
        //
        //      if(file_exists(SINGLE_PATH . '/single-' . $cat->slug . '.php'))
        //           return SINGLE_PATH . '/single-' . $cat->slug . '.php';
        //
        //      else if(file_exists(SINGLE_PATH . '/single-' . $cat->term_id . '.php'))
        //           return SINGLE_PATH . '/single-' . $cat->term_id . '.php';
        //
        //      endforeach;
        // }
      /**
      * Hàm hiển thị tag của post
      * blog_entry_tag()
      **/
      if ( ! function_exists( 'blog_entry_tag' ) ) {
        function blog_entry_tag() {
          if ( has_tag() ) :
            //echo '<div class="entry-tag">';
           // printf( __('Tagged in %1$s', 'blog-language'), get_the_tag_list( '', ', ' ) );
            //echo '</div>';
          endif;
        }
      }



  /**
  * Tính lượt View cho bài viết
  */

  function setpostview($postID){
    $count_key ='views';
    $count = get_post_meta($postID, $count_key, true);
    if($count == ''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    } else {
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
  }
  function getpostviews($postID){
      $count_key ='views';
      $count = get_post_meta($postID, $count_key, true);
      if($count == ''){
          delete_post_meta($postID, $count_key);
          add_post_meta($postID, $count_key, '0');
          return "0";
      }
      return $count;
  }

  /**
  ** Tao widget Logo
  **/
  if ( function_exists('register_sidebar') ){
    register_sidebar(array(
        'name' => 'web_header_logo',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '',
        'after_title' => '',
  ));
  }

  /** Thay doi duong dan Logo Login */
  // Thay doi duong dan logo admin
  function wpc_url_login(){
  return "http://phantrongbien.com/";
  }
  add_filter('login_headerurl', 'wpc_url_login');
  /** Chen file CSS cho Login */
  // Thay doi logo admin wordpress
  function login_css() {
  wp_enqueue_style( 'login_css', get_template_directory_uri() . '/assets/css/login.css' ); // duong dan den file css moi
  }
  add_action('login_head', 'login_css');

  /**
   * Insert CSS và Javascript
   * Using hook wp_enqueue_scripts() de hien thi ra Front-end
  **/
  function blog_styles() {
    /*
    * Funcion get_stylesheet_uri() se tra ve file main.css cua theme
    */
    // import CSS

    wp_register_style('main-style', get_template_directory_uri() . '/assets/css/main.css', 'all' );
    wp_enqueue_style('main-style' );

    // import JS
    wp_enqueue_script('lib-jquery', get_template_directory_uri() . '/assets/js/jquery.js', array('jquery'));
    wp_enqueue_script('lib-jquery');

    // wp_enqueue_script('timeline-jquery', get_template_directory_uri() . '/assets/js/timeline.js', array('jquery'));
    // wp_enqueue_script('timeline-jquery');

    wp_enqueue_script('main-js', get_template_directory_uri() . '/assets/js/scripts.js', array('jquery'));
    wp_enqueue_script('main-js');
  }
  add_action( 'wp_enqueue_scripts', 'blog_styles' );

  //* Move JavaScript to the Footer
  //* Dam bao viec load Js ko bi loi
  function remove_head_scripts() {
     remove_action('wp_head', 'wp_print_scripts');
     remove_action('wp_head', 'wp_print_head_scripts', 9);
     remove_action('wp_head', 'wp_enqueue_scripts', 1);
     add_action('wp_footer', 'wp_print_scripts', 5);
     add_action('wp_footer', 'wp_enqueue_scripts', 5);
     add_action('wp_footer', 'wp_print_head_scripts', 5);
  }
  add_action( 'wp_enqueue_scripts', 'remove_head_scripts' );
?>
