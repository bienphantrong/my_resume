<?php
/*
 Template Name: Portofolio
 */
 ?>
  <!DOCTYPE html>
  <!--[if IE 8]> <html <?php language_attributes(); ?> class="ie8"> <![endif]-->
  <!--[if !IE]> <html <?php language_attributes(); ?>> <![endif]-->

  <head>
  	<meta charset="<?php bloginfo( 'charset' ); ?>" />
  	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
  	<meta name="viewport" content="width=device-width, initial-scale=1">
  	<meta name="description" content="Webdesigner, front-end developer" />
  	<link rel="profile" href="http://gmgp.org/xfn/11" />
  	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

   	<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon.png" />

  	<!-- Web Fonts -->
  	<link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet">
    <?php wp_head(); ?>
  </head>

  <body>
  	<!--menu Responsive-->
  	<div class="menu-responsive">
  	  <?php blog_menu( 'primary-menu' ); ?>
  	</div>
  	<button type="button" class="menuBtn">
  	  <span></span>
  	</button>

  	<button type="button" class="hideBtn">
  	  <span></span>
  	</button>
  	<!--end menu Responsive-->

  	<!--header show mobile-->
    <div class="header-mobile">
      <div class="header-mobile__avata">
        <?php dynamic_sidebar('web_header_logo');  ?>
      </div>

      <div class="header-mobile__name">
        <p class="header-mobile__name--main">Phan Trong Bien</p>
        <p class="header-mobile__name--sub">Ui/Ux Designer & Front-end developer</p>
      </div>

    </div>
    <!--end header mobile-->
  	<!--aside-->
  	<aside class="blog-aside">

  		<div class="blog-aside__avata">
  			<?php dynamic_sidebar('web_header_logo');  ?>
  		</div>
      <p class="txt_name">Phan Trong Bien</p>
      <p class="txt_position">UI/UX Designer & Front-end Developer</p>

  		<div class="blog-aside__menu">
  			<?php blog_menu( 'primary-menu' ); ?>
  		</div>

  		<div class="blog-aside__footer">
  			<span class="copyright">© 2018. All rights reserved</span>
  		</div>

  	</aside>
  	<!--aside-->
  	<!--blog-main-->
  	<div class="blog-main" style="height: 100vh; background: #fff;">
      <!-- portfolio list-->
      <div class="portfolio-content">
        <?php
        if ( function_exists( 'envira_gallery' ) ) { envira_gallery( '142' ); }
        ?>
      </div>
      <!--end-->
   </div>
   <!--e blog-main-->
   <?php wp_footer(); ?>

 </body>
 </html>
