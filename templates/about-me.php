<?php
/*
 Template Name: About Me
 */
 ?>
 <!DOCTYPE html>
 <!--[if IE 8]> <html <?php language_attributes(); ?> class="ie8"> <![endif]-->
 <!--[if !IE]> <html <?php language_attributes(); ?>> <![endif]-->

 <head>
 	<meta charset="<?php bloginfo( 'charset' ); ?>" />
 	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
 	<meta name="viewport" content="width=device-width, initial-scale=1">
 	<meta name="description" content="Webdesigner, front-end developer" />
 	<link rel="profile" href="http://gmgp.org/xfn/11" />
 	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

  	<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon.png" />

 	<!-- Web Fonts -->
 	<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,600,700,900" rel="stylesheet">
 	<link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet">
 	<?php wp_head(); ?>

  <style>
    .skill-row__label{
      font-size: 13px;
      width: 80%;
      float: left;
    }
    div.meter{
      width: 100%;
    }
  </style>
 </head>

 <body>
 	<!--menu Responsive-->
 	<div class="menu-responsive">
 	  <?php blog_menu( 'primary-menu' ); ?>
 	</div>
 	<button type="button" class="menuBtn">
 	  <span></span>
 	</button>

 	<button type="button" class="hideBtn">
 	  <span></span>
 	</button>
 	<!--end menu Responsive-->

 	<!--header show mobile-->
   <div class="header-mobile">
     <div class="header-mobile__avata">
       <?php dynamic_sidebar('web_header_logo');  ?>
     </div>

     <div class="header-mobile__name">
       <p class="header-mobile__name--main">Phan Trong Bien</p>
       <p class="header-mobile__name--sub">Ui/Ux Designer & Front-end developer</p>
     </div>

   </div>
   <!--end header mobile-->
 	<!--aside-->
 	<aside class="blog-aside">

 		<div class="blog-aside__avata">
 			<?php dynamic_sidebar('web_header_logo');  ?>
 		</div>
     <p class="txt_name">Phan Trong Bien</p>
     <p class="txt_position">UI/UX Designer & Front-end Developer</p>

 		<div class="blog-aside__menu">
 			<?php blog_menu( 'primary-menu' ); ?>
 		</div>

 		<div class="blog-aside__footer">
 			<span class="copyright">© 2018. All rights reserved</span>
 		</div>

 	</aside>
 	<!--aside-->
 	<!--blog-main-->
 	<div class="blog-main">

    <!--Skill content-->
    <div class="about-page">
      <!--about title-->
      <div class="about-page__title">
        <h2>About Me</h2>
        <p></p>
        <span class="text-background">About Me</span>
      </div>
      <!--end about title-->

      <!--about content-->
      <div class="about__content">
        <div class="about__avata">
          <img src="http://phantrongbien.com/wp-content/uploads/2018/06/33748355_785689571630033_2876938380996771840_n.jpg" alt="avata" />
        </div>
        <div class="about__des">
          <h2 class="my-name">PHAN TRONG BIEN</h2>
          <h4>UI/UX Designer & Front-end Developer</h4>
          <p>Hello, my name is Bien, I am 30 years old and married. I am currently living and working in Hanoi.</p>
          <p>I am working as an UI/UX Designer & Front-end Developer. I enjoy to work with creative products, therefore I choose UI/ UX Design Field.</p>
          <p>I have 4-year experiences working as UI Design và Front-end Developer. Besides, I make code Mobile App UI using React Native, and sometimes I make code theme Wordpress.</p>
          <p>With respect to foreign language, I can use Japanese for communication but that is not really good. I will spend more time upgrading my Japanese in next time.</p>
          <!--a href="">Download My CV</a-->
          <div class="my-follow">
            <div class="my-follow__items">
              <span>fb.com/phantrongbien</span>
            </div>
            <div class="my-follow__items">
              <span>skype: phantrongbien</span>
            </div>
            <div class="my-follow__items">
              <span>phantrongbien@gmail.com</span>
            </div>
            <div class="my-follow__items">
              <span>08-68-61-69-68</span>
            </div>
          </div>

        </div>
      </div>
      <!--end about content-->
    </div>
    <!--e about content-->

    <!--SKILLs content-->
    <div class="about-page">
      <!--about title-->
      <div class="about-page__title">
        <h2>My Skills</h2>
        <span class="text-background">My Skills</span>
      </div>
      <!--end about title-->
      <!--content-->
      <div class="about-page__content">

        <!--top skill-->
        <div class="top-skill">

          <!--top skill items-->
          <div class="top-skill__items">
            <div class="top-skill__icon">
              <img src="http://phantrongbien.com/wp-content/uploads/2018/06/design.png" alt="icon skill" />
            </div>
            <div class="top-skill__text">
              <h4>Ui/ux Design</h4>
              <p>Prototype visual design concepts, UI/UX design, mobile app design, website design...</p>
            </div>
          </div>
          <!--e top skill items-->

          <!--top skill items-->
          <div class="top-skill__items">
            <div class="top-skill__icon">
              <img src="http://phantrongbien.com/wp-content/uploads/2018/06/code.png" alt="icon skill" />
            </div>
            <div class="top-skill__text">
              <h4>Front-end develop</h4>
              <p>Web front-end, mobile app ui, wordpress theme...</p>
            </div>
          </div>
          <!--e top skill items-->

          <!--top skill items-->
          <div class="top-skill__items">
            <div class="top-skill__icon">
              <img src="http://phantrongbien.com/wp-content/uploads/2018/06/group.png" alt="icon skill" />
            </div>
            <div class="top-skill__text">
              <h4>Teamwork Skills</h4>
              <p>Communication, Reliability, Listening, Conflict Management, Respectfulness...Also, I have experience with Agile, Scrum</p>
            </div>
          </div>
          <!--e top skill items-->

        </div>
        <!--end top skill-->
        <div class="skill-detail">
          <div class="about-page__left">

            <!--progress bar-->
            <div class="skill-row">
              <div class="skill-row__label"><span>01. Photoshop/Figma/Sketch</span></div>
              <div class="meter meter--blue">
                <span style="width:90%"></span>
                <p></p>
              </div>
              <!--end-->
            </div>
            <!--end progress bar-->

            <!--progress bar-->
            <div class="skill-row">
              <div class="skill-row__label"><span>02. HTML/CSS/Jquery</span></div>
              <div class="meter">
                <span style="width:90%"></span>
                <p></p>
              </div>
              <!--end-->
            </div>
            <!--end progress bar-->

            <!--progress bar-->
            <div class="skill-row">
              <div class="skill-row__label"><span>03. Bootstrap/Sass/Gulp</span></div>
              <div class="meter">
                <span style="width:90%"></span>
                <p></p>
              </div>
              <!--end-->
            </div>
            <!--end progress bar-->
          </div>
          <div class="about-page__right">

            <!--progress bar-->
            <div class="skill-row">
              <div class="skill-row__label"><span>04. Wordpress</span></div>
              <div class="meter">
                <span style="width:50%"></span>
                <p></p>
              </div>
              <!--end-->
            </div>
            <!--end progress bar-->

            <!--progress bar-->
            <div class="skill-row">
              <div class="skill-row__label"><span>05. ReactJs/Angular</span></div>
              <div class="meter">
                <span style="width:40%"></span>
                <p></p>
              </div>
              <!--end-->
            </div>
            <!--end progress bar-->

            <!--progress bar-->
            <div class="skill-row">
              <div class="skill-row__label"><span>06. Git</span></div>
              <div class="meter meter--orange">
                <span style="width:40%"></span>
                <p></p>
              </div>
              <!--end-->
            </div>
            <!--end progress bar-->

            <!--progress bar-->
            <div class="skill-row">
              <div class="skill-row__label"><span>07. Japanese</span></div>
              <div class="meter meter--pure">
                <span style="width:30%"></span>
                <p></p>
              </div>
              <!--end-->
            </div>
            <!--end progress bar-->
          </div>

        </div>
      </div>
      <!--e content-->
    </div>
    <!--e Skill content-->


    <!--about content-->
    <div class="about-page">

      <div class="about-page__title">
        <h2>Experience</h2>
        <span class="text-background">Experience</span>
      </div>

      <div class="about-page__content">
        <section class="cd-horizontal-timeline">
        	<div class="timeline">
        		<div class="events-wrapper">
        			<div class="events">
        				<ol>
        					<li><a href="#0" data-date="15/01/2018" class="selected">2018</a></li>
        					<li><a href="#0" data-date="15/02/2018">2016-2017</a></li>
        					<li><a href="#0" data-date="15/03/2018">2015</a></li>
        					<li><a href="#0" data-date="15/04/2018">2012-2014</a></li>
        				</ol>

        				<span class="filling-line" aria-hidden="true"></span>
        			</div> <!-- .events -->
        		</div> <!-- .events-wrapper -->

        		<ul class="cd-timeline-navigation">
        			<li><a href="#0" class="prev inactive">Prev</a></li>
        			<li><a href="#0" class="next">Next</a></li>
        		</ul> <!-- .cd-timeline-navigation -->
        	</div> <!-- .timeline -->

        	<div class="events-content">
        		<ol>
        			<li data-date="15/01/2018" class="selected">
        				<h2>UI/UX Designer & Front-end Developer</h2>
                <span class="company-detail">
                  <span class="logo-company">
                    <a href="https://vnext.vn/" target="_blank"><img src="https://vnext.vn/img/logo-vnext-white.svg" alt="vnext logo"  /></a>
                  </span>
                  <span class="company-des">
                    <a href="https://vnext.vn/" target="_blank">Vnext Software</a>
                    <p>
            					I express my wish to work together and learn working style of Japanese.
                      In early 2018, I have attended Vnext as role of one UI/UX Designer & Front-end Developer.
                      Our team are developing a very interesting project for Corporation in Japan, I and my team put all faith in this product.
                    </br>At this place, I have learned a lot hope that I myself will devote and make long relationship with Vnext Software.</p>
                    <p>Position  : <strong>UI/UX Designer, Front-end Developer.</strong></p>
                    <p>Skills : <strong>UI/UX Design, Photoshop, Figma, HTML, CSS, SASS, Bootstrap, Jquery, Angular 5, Git...</strong></p>

                  </span>
                </span>
        			</li>

        			<li data-date="15/02/2018">
                <h2>Mobile App Designer & Front-end Developer</h2>
                <span class="company-detail">
                  <span class="logo-company shippo">
                    <a href="https://shippo.vn/" target="_blank"><img src="http://shippo.vn/wp-content/themes/shippo-homepage/img/logo-shippo.png" alt="Shippo logo"  /></a>
                  </span>
                  <span class="company-des">
                    <a href="https://shippo.vn/" target="_blank">Shippo</a>
                    <p>
                      Để có cơ hội được học hỏi nhiều hơn, tôi đã cùng gia đình chuyển lên Hà Nội sinh sống và làm việc. </br>
                      Và Shippo là công ty đầu tiên của tôi làm việc tại Hà Nội. </br>
                      Ở Shippo, tôi đã thay đổi rất nhiều từ kỹ năng công việc (học hỏi nhiều ngôn ngữ mới), kỹ năng giao tiếp và đặc biệt được làm việc trong Team áp dụng Agile, điều làm tôi
                      thấy thực sự thú vị trong vai trò cùng Team phát triển Product cho chính Shippo. </br>
                      Bên cạnh đó, Quản lí của tôi cùng các thành viên trong Team thực sự gắn kết, cùng chia sẻ và giúp đỡ nhau trong công việc. Tôi rất vui vì mình có những
                      đồng đội như vậy.</br>
                      Sau 1 thời gian, với mong muốn thực hiện mục tiêu cá nhân, tôi đã rời Shippo để tìm môi trường mới.
            				</p>
                    <p>Vị trí : <strong>Mobile App Designer, Front-end Developer.</strong></p>
                    <p>Kỹ năng : <strong>UI/UX Design, Sketch, HTML, CSS, SASS, Bootstrap, ReactJs, React Native, Git...</strong></p>

                  </span>
                </span>
        			</li>

        			<li data-date="15/03/2018">
                <h2>Thực tập sinh tại Oita - Nhật Bản</h2>
                <span class="company-detail">
                  <span class="logo-company">
                    <a href="#" target="" style="font-size: 15px; color: #fff;">TeitoGomu</a>
                  </span>
                  <span class="company-des">
                    <a href="#">Teito Gomu</a>
                    <p>
                      Sau một thời gian làm việc với vị trí UI Designer & HTML developer, tôi nhận thấy bản thân thiếu rất nhiều thứ khiến công việc cũng như sự nghiệp bản thân khó có thể phát triển thêm nữa.
                      Vì vậy tôi quyết định dừng lại 1 thời gian để xác định rõ lại mục tiêu của bản thân.
                    </p>
                    <p>
                      Tôi quyết định sang Nhật Bản làm việc 1 năm, vì chưa biết tiếng Nhật và kỹ năng IT cũng chưa thực sự tốt nên tôi chọn con đường Thực Tập Sinh tại Nhật Bản.
                      Thực tế là làm công nhân trong nhà máy đúc cao su cho linh kiện Ôto, công việc thực sự vất vả với 1 người làm văn phòng như tôi.
                      Rất nóng, nặng và cường độ làm việc cao.</p>
                    <p>Tuy nhiên, lựa chọn của tôi là mong muốn được học hỏi phong cách làm việc của người Nhật.
                      Sự chăm chỉ, tận tâm làm việc hết mình của người Nhật đã giúp tôi thay đổi suy nghĩ bản thân.
                      Tôi dần thích nghi với công việc và đã làm tốt nhiệm vụ của mình.
                      Hàng ngày làm việc và quan sát, học hỏi họ, để nhận thấy THÁI ĐỘ thực sự quan trọng trong công việc. </p>
                    <p>Kết thúc 1 năm làm việc tại Nhật Bản, tôi về nước và chuẩn bị cho mình cho mình mục tiêu công việc sắp tới, trở lại với ngành IT mà tôi đang làm việc.
                      Tôi tin rằng, thay đổi Thái Độ làm việc, học tập cũng như tìm cho bản thân môi trường làm việc Phù Hợp chắc chắn sẽ giúp tôi thành công. </p>
            				</p>
                    <p>Vị trí : <strong>Nhân viên sản xuất linh kiện Oto.</strong></p>
                    <p>Kỹ năng : <strong>Đút và Rút cao su từ lò nung.</strong></p>

                  </span>
                </span>
        			</li>

        			<li data-date="15/04/2018">
                <h2>UI Designer & Front-end Developer</h2>
                <span class="company-detail cnet">
                  <span class="logo-company">
                    <a href="http://cnet.edu.vn" target="_blank"><img src="http://cnet.edu.vn/wp-content/uploads/2017/08/logo-web-fix.png" alt="cnet logo"  /></a>
                  </span>
                  <span class="company-des">
                    <a href="http://cnet.edu.vn" target="_blank">Cnet Academy</a>
                    <p>
                      Sau 1 thời gian tốt nghiệp, tôi lựa chọn cho mình con đường trở thành một UI Designer & Front-end Developer tại Hải Phòng.
                      Hoàn toàn không có định hướng rõ ràng, tôi đã bắt đầu từ việc tự học và rèn luyện kỹ năng qua các dự án thực tế.</p>
                    <p>
                      Tại Cnet Academy, tôi tham gia với vai trò Web designer và HTML developer.
                      Bên cạnh đó, Cnet mở các lớp đào tạo thiết kế và lập trình website, tôi tham gia giảng dạy môn thiết kế giao diện Website.

            			  </p>
                    <p>Vị trí : <strong>Web Designer, HTML Developer.</strong></p>
                    <p>Kỹ năng : <strong>Photoshop, HTML, CSS, Jquery.</strong></p>

                  </span>
                </span>
        			</li>

        		</ol>
        	</div> <!-- .events-content -->
        </section>
      </div>

    </div>
    <!--e Skill content-->



  </div>
  <!--e blog-main-->
  <?php wp_footer(); ?>
  <script type='text/javascript' src='http://phantrongbien.com/wp-content/themes/my_resume/assets/js/timeline.js?ver=4.9.5'></script>
</body>
</html>
