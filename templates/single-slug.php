<?php get_header(); ?>
<?php setpostview(get_the_id()); ?>
  <!--content contact-->
	<div class="container">
		<div class="row">

      <!--content-->
      <div class="page__content single-page__content">
        <?php
        // Start the loop.
        while ( have_posts() ) : the_post(); ?>

					<h1><?php the_title(); ?></h1>
					<div class="more-info">
						<span class="post-time">
							<span class="post-time__label">Thời gian :</span>
							<?php
							echo '<span class="post-time__value">';
								$post_date = get_the_date( 'j/m/' );
								$post_y = get_the_date( 'Y' );
									echo $post_date ;
									echo $post_y ;
							echo '</span>';
							?>
							<span class="post-time__line"> - </span>
							<span class="post-time__label">Lượt xem :</span><span class="post-time__value"><?php echo getpostviews(get_the_id()); ?></span>
						</span>
					</div>
					<div class="post-content">
						<?php the_content(); ?>
					</div>

				<?php
				endwhile;
        ?>
      </div>
      <!--content-->
      <!--content-->
      <div class="page__sidebar">
        <?php
          $categories = get_the_category($post->ID);
          if ($categories)
            {
                $category_ids = array();
                foreach($categories as $individual_category) $category_ids[] = $individual_category->term_id;

                $args=array(
                'category__in' => $category_ids,
                'post__not_in' => array($post->ID), // bỏ qua bài viết hiện tại
                'showposts'=>99,
                'caller_get_posts'=>1
                );
                $my_query = new wp_query($args);
                if( $my_query->have_posts() )
                {
                  echo '
                  <div class="page__sidebar__title">
                    <span>Các bài viết liên quan</span>
                  </div>
                  <div class="page__sidebar__content">
                    <ul>';
                        while ($my_query->have_posts())
                        {
                            $my_query->the_post();
                            ?>
                            <li>
															<div class="post-img">
								                <a href="<?php the_permalink() ?>" title="<?php the_title(); ?>">
								                  <?php the_post_thumbnail(90,90); ?>
								                </a>
								              </div>
								              <div class="post-link">
								                <a href="<?php the_permalink() ?>" title="<?php the_title(); ?>">
								                  <?php the_title(); ?>
								                </a><br/>
								                <span class="post-time">
								                  <span class="post-time__label">Thời gian :</span>
								                  <?php
								                  echo '<span class="post-time__value">';
								                    $post_date = get_the_date( 'j/m/' );
								                    $post_y = get_the_date( 'Y' );
								                      echo $post_date ;
								                      echo $post_y ;
								                  echo '</span>';
								                  ?>
								                  <span class="post-time__line"> - </span>
								                  <span class="post-time__label">Lượt xem :</span><span class="post-time__value"><?php echo getpostviews(get_the_id()); ?></span>
								                </span>
								              </div>
                            </li>
                            <?php
                        }
                        echo '
                        </ul>
                      </div>';
                    }
                }
              ?>

      </div>
      <!--content-->

		</div>
	</div>
	<!--end gg map-->
  <!--end content contact-->
<?php get_footer(); ?>
