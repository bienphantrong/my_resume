<?php get_header(); ?>
  <!--blog content-->
  <div class="blog-main__content">

    <!--page title-->
    <div class="blog-main__title">
			<h1>All Blog Articles</h1>
			<p>Ui-ux Design, Front-end Developer, Wordpress, Reactjs, React Native...</p>
		</div>
    <!--end page title-->
    <?php
      $args = array( 'post_type' =>  'post', 'order' => 'DESC', 'posts_per_page' => 12  );
      $postslist = get_posts( $args );
      foreach ($postslist as $post) :  setup_postdata($post);
    ?>
      <!--blog items-->
      <div class="blog-main__post">
        <!--img-->
        <?php if (has_post_thumbnail( $post->ID ) ): ?>
          <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
          <a href="<?php the_permalink() ?>" title="<?php the_title(); ?>">
            <div class="blog-main__post-avata" style="background-image: url('<?php echo $image[0]; ?>');">
            </div>
          </a>
        <?php endif; ?>
        <!--e img-->
        <div class="card__tag-post">
          <?php the_tags( ' ', '  ', '<br />' ); ?>
        </div>
        <!--link-->
        <div class="blog-main__post-link" style="fsfs">
          <a href="<?php the_permalink() ?>" title="<?php the_title(); ?>">
            <?php the_title(); ?>
          </a>
        </div>
        <!--des-->
        <div class="blog-main__post-des" style="overflow: hidden;-o-text-overflow: ellipsis;text-overflow: ellipsis;display: -webkit-box;-webkit-line-clamp: 3;-webkit-box-orient: vertical;">
          <?php the_excerpt(); ?>
        </div>
        <!--more-->
        <div class="blog-main__post-ico">
          <div class="post-icon">
             <?php
                $category_detail=get_the_category(get_the_id());//$post->ID
                foreach($category_detail as $cd){
             ?>
                 <img src="<?php echo z_taxonomy_image_url($cd->term_id); ?>" class="icon-post" alt="icon category"/>
            <?php } ?>
          </div>
          <span class="post-time">
            <?php
             $post_d = get_the_date( 'j' );
             $post_m = get_the_date( 'm' );
             $post_y = get_the_date( 'Y' );
             if($post_d > 9){
               echo $post_d;
             }else echo '0'.$post_d;
             echo ' - '.$post_m;
             echo ' - '.$post_y ;
           ?>
          </span>
          <span class="post-line">|</span>
          <span class="post-view-counter">
            Lượt xem :
            <span style="color: #20b4c9; margin-left: 5px;">
              <?php echo getpostviews(get_the_id()); ?>
            </span>
          </span>
        </div>
        <!--end more-->
      </div>
      <!--end blog items-->
    <?php endforeach; ?>

  </div>
  <!--end blog content-->
  <!-- e index content-->
<?php get_footer(); ?>
